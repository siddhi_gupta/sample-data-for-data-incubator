import itertools
import numpy as np
from operator import mul
from decimal import Decimal, getcontext
getcontext().prec = 10
n = 1
t = 4
a = 2048
b = 4096
countAB = 0
countB = 0
mlDiff = []
for val in itertools.product(np.arange(1, 11, 1), repeat = t):
	print val
	lProd = reduce(mul, val[-n:])
	val = sorted(val)
	mProd = reduce(mul, val[-n:])
	diff = mProd - lProd
	if diff >= a and diff <= b:
		countAB += 1
	if diff <= b:
		countB += 1
	mlDiff.append(diff)
	del val
mean1 = np.mean(mlDiff)
print mean1
std1 = np.std(mlDiff, ddof=1)
print std1
condProbability = Decimal(countAB) / Decimal(countB)
print condProbability
# N = 2, T = 8, A = 32, B = 64 MEAN = 47.71416969, STD = 24.8002773495, Conditional Probability = 0.6173959116 
# N = 4, T = 32, A = 2048, B = 4096 MEAN = , STD = , Conditional Probability = 