from bs4 import BeautifulSoup
import urllib2
from cookielib import CookieJar
import json
f = open ('diseases.txt','w')
cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
res = opener.open("http://www.brainfacts.org/diseases-disorders/diseases-a-to-z-from-ninds/")
html_code = res.read()
soup = BeautifulSoup(html_code, 'html.parser')
first = soup.find("div", {"class": "tab-pane active"})
second = first.find("ul")
third = second.find_all("li")
for each in third:
    name = (each.find("a").contents[0]).encode("utf-8")
    nextpage = opener.open(each.find("a")['href'])
    html_code1 = nextpage.read()
    soup1 = BeautifulSoup(html_code1, 'html.parser')
    treatment = soup1.find("div",{"id":"leftcolumn_0_divTreatment"})
    treat =  treatment.find("p").contents[0]
    prognosis = soup1.find("div",{"id":"leftcolumn_0_divPrognosis"})
    progno =  prognosis.find("p").contents[0]
    ans = {}
    ans["name"] = name
    ans["treatment"] = treat
    ans["prognosis"] = progno
    print json.dumps(ans).encode('utf-8')
    f.write(json.dumps(ans).encode('utf-8')+'\n')
f.close()